<?php
namespace Commands;

/**
 * Interface CommandInterface
 * This interface must fit each Command what we want to run separately from command line
 *
 * @package Commands
 */
interface CommandInterface
{
    /**
     * Key function for executing command
     *
     * @return mixed
     */
    public function run();
}