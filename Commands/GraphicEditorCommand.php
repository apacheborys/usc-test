<?php
namespace Commands;

use Entity\ComputedShape;
use Entity\Params;
use Entity\ShapesAbstract;
use Entity\ShapesInterface;
use Processor\ProcessorAbstract;
use Processor\ProcessorInterface;
use Service\JsonProcService;
use Service\StoreService;

class GraphicEditorCommand extends CommandAbstract
{
    public function __construct()
    {
        parent::__construct();
    }

    public function run()
    {
        $currentTime = new \DateTime();

        $this->logService->info('Start execution graphic editor');

        if (!isset($argv[2])) {
            throw new \Exception('Please provide json encode argument what describe shape');
        }

        /**
         * Parse string to array
         */
        $jsonProcService = new JsonProcService($argv[2]);
        if (!$jsonProcService->resultArray) {
            throw new \Exception('Mailformed json data: ' . $jsonProcService->lastError);
        }

        $this->logService->info('Parsed string OK');

        /**
         * Validation
         */
        foreach ($jsonProcService->resultArray as $indexShape => $rawShape) {
            if (!isset($rawShape['type'])) {
                throw new \Exception('In ' . $indexShape . ' element can\'t find "type" element');
            }

            if (!class_exists('Entity\\' . $rawShape['type'])) {
                throw new \Exception('Can\'t find this type of shape');
            }

            if (!isset($rawShape['params'])) {
                throw new \Exception('In ' . $indexShape . ' element can\'t find "params" element');
            }
        }

        $this->logService->info('Validate data in array OK');

        /**
         * Mapping
         *
         * @var ShapesAbstract[] $preparedShapes
         * @var ShapesAbstract $tempShape
         */
        $preparedShapes = [];
        foreach ($jsonProcService->resultArray as $rawShape) {
            $rawShape['type'] .= 'Entity\\';

            $tempShape = new $rawShape['type']();
            $tempParams = new Params();
            foreach ($rawShape['params'] as $paramName => $param) {
                $tempParams->{'set' . ucfirst($paramName)}($param);
            }

            $tempShape->setParams($tempParams);

            $preparedShapes[] = $tempShape;
        }

        $this->logService->info('Map data in array to entities OK');

        /**
         * Processing
         *
         * @var ProcessorAbstract $tempProc
         * @var ComputedShape[] $computedShapes
         */
        $computedShapes = [];
        foreach ($preparedShapes as $shape) {
            $tempProcName = 'Processor\\Processor' . $shape->getType();
            $tempProc = new $tempProcName();
            $computedShapes[] = $tempProc->processData($shape);
        }

        $this->logService->info('Compute entities to images OK');

        /**
         * Make store action
         */
        $dirName = $currentTime->format('Y-m-d H:i:s');
        $clientS3 = new StoreService();
        $clientS3->createBucket($dirName);

        $savedQty = 0;
        foreach ($computedShapes as $index => $shape) {
            if ($clientS3->send($dirName . '/' . $index . '.png', $shape->getBinaryData(), 1)) {
                $savedQty++;
            }
        }

        $this->logService->info('Generated: ' . count($computedShapes) . ', saved: ' . $savedQty);

        $this->logService->info('Finish working');
    }
}