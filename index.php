<?php

function libraryRoot($classname) {
    $filename = str_replace('\\',DIRECTORY_SEPARATOR, $classname) .".php";
    require_once($filename);
}

spl_autoload_register('libraryRoot');

if (!isset($argv[1])) {
    throw new Exception('Please provide in first argument full class name of Command interface');
}

/**
 * @var \Commands\CommandInterface $entryPoint
 */
$entryPoint = new $argv[1]();

try {
    $entryPoint->run();
} catch (Exception $exception) {
    $logService = new \Service\LogService();
    $logService->error(
        'Panic ending of ' .
        $entryPoint . ' application! ' . $exception->getMessage() .
        '. Trace: ' . $exception->getTraceAsString());
}
