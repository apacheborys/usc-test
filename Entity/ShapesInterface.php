<?php
namespace Entity;

/**
 * Interface ShapesInterface
 * Describe fundamentally function for properly work each shape
 *
 * @package Entity
 */
interface ShapesInterface
{
    /**
     * @return string
     */
    public function getType():string;

    /**
     * @return Params
     */
    public function getParams():Params;

    /**
     * @param Params $params
     * @return mixed
     */
    public function setParams(Params $params);
}