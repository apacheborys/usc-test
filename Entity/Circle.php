<?php
namespace Entity;

/**
 * Class Circle
 *
 * @package Entity
 */
class Circle extends ShapesAbstract
{
    private $type = 'circle';
}