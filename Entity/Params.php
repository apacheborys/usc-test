<?php
namespace Entity;

/**
 * Class Params
 * In this entity you must describe parameters (properties) of your shapes
 *
 * @package Entity
 */
class Params
{
    /**
     * Color name
     *
     * @var string $color
     */
    private $color;

    /**
     * Border color
     *
     * @var string $border
     */
    private $border;

    /**
     * Width of shape
     *
     * @var int $width
     */
    private $width;

    /**
     * Height of shape
     *
     * @var int $height
     */
    private $height;

    /**
     * Array of x,y coordinates what must be connected by line in ascending order
     *
     * @var array $coordinates
     */
    private $coordinates;

    /**
     * @param string $color
     */
    public function setColor(string $color)
    {
        $this->color = $color;
    }

    /**
     * @param string $border
     */
    public function setBorder(string $border)
    {
        $this->border = $border;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width)
    {
        $this->width = $width;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    /**
     * @param array $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }

    /**
     * Getter all parameters
     *
     * @return array
     */
    public function getParams():array
    {
        return get_class_vars(self::class);
    }
}