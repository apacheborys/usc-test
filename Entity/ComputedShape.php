<?php
namespace Entity;

/**
 * Class ComputedShape
 * Computed entity what will be save ready
 *
 * @package Entity
 */
class ComputedShape
{
    /**
     * Type of shape
     *
     * @var string $type
     */
    private $type;

    /**
     * Computed binaryData
     *
     * @var string $binaryData
     */
    private $binaryData;

    /**
     * Mime type of binary data
     *
     * @var string $mimeType
     */
    private $mimeType;

    /**
     * @return string
     */
    public function getType():string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getBinaryData():string
    {
        return $this->binaryData;
    }

    /**
     * @param string $binaryData
     */
    public function setBinaryData(string $binaryData)
    {
        $this->binaryData = $binaryData;
    }

    /**
     * @return string
     */
    public function getMimeType():string
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType(string $mimeType)
    {
        $this->mimeType = $mimeType;
    }
}