<?php
namespace Entity;

/**
 * Class ShapesAbstract
 * Abstract class what implement basic functions for manipulate with shape entities
 *
 * @package Entity
 */
abstract class ShapesAbstract implements ShapesInterface
{
    /**
     * Type of shape
     *
     * @var string $type
     */
    private $type;

    /**
     * Collection of parameters what can describe entity and help to make all computing
     *
     * @var Params $params
     */
    private $params;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Params
     */
    public function getParams():Params
    {
        return $this->params;
    }

    /**
     * @param Params $params
     */
    public function setParams(Params $params): void
    {
        $this->params = $params;
    }
}