<?php
namespace Service;

/**
 * Class StoreService
 * It's only example, doesn't real...
 *
 * @package Service
 */
class StoreService
{
    /**
     * Connection credentials. Better idea to take out it to .env file what ignored by git
     */
    const LOGIN = 'login';
    const PASS = 'pass';
    const HOST = 'host';

    /**
     * Our client of abstract connection. For example it will be cloud storage on Amazon (S3)
     *
     * @var ClientS3
     */
    private $clientS3;

    /**
     * StoreService constructor.
     */
    public function __construct()
    {
        $this->clientS3 = new ClientS3(['login' => self::LOGIN, 'pass' => self::PASS, 'host' => self::HOST]);
    }

    /**
     * Create new directory
     *
     * @param string $address
     * @return bool
     */
    public function createBucket(string $address)
    {
        return $this->clientS3->createBucket($address);
    }

    /**
     * Delete specific directory
     *
     * @param string $address
     * @return mixed
     */
    public function deleteBucket(string $address)
    {
        return $this->clientS3->deleteBucket($address);
    }

    /**
     * @param string $address
     * @param string $binaryData
     * @param int $connectionType
     * @return bool|string
     */
    public function send(string $address, string $binaryData, int $connectionType)
    {
        return $this->clientS3->sendData($address, $binaryData, $connectionType);
    }

    /**
     * Delete some specific data
     *
     * @param string $address
     * @return mixed
     */
    public function deleteNode(string $address)
    {
        return $this->clientS3->delete($address);
    }

    /**
     * Important for all kinds network connection - drop connection for ensure absent leaking connections
     */
    public function __destruct()
    {
        $this->clientS3->disconnect();
    }
}