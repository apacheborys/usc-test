<?php
namespace Service;

/**
 * Class LogService
 * Something abstract class with possibility to store different type events
 *
 * @method bool error(string $eventString) Write error message
 * @method bool warning(string $eventString) Write warning message
 * @method bool info(string $eventString) Write info message
 * @package Service
 */
class LogService
{
    /**
     * Connection credentials. Better idea to take out it to .env file what ignored by git
     */
    const LOGIN = 'login';
    const PASS = 'pass';
    const HOST = 'host';

    /**
     * @var SomeDbClient $clientDb
     */
    private $clientDb;

    /**
     * LogService constructor.
     */
    public function __construct()
    {
        $this->clientDb = new SomeDb('protocol://' . self::LOGIN . ':' . self::PASS . '@' . self::HOST);
    }

    /**
     * @param $name
     * @param $arguments
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if ($name !== 'error' && $name !== 'warning' && $name !== 'info') {
            throw new \Exception('Undefined call of LogService');
        }

        $this->clientDb->write('logEntity', $name . 'Type', $arguments[0]);

        error_log('[' . ucfirst($name) . '] ' . date('c') . ': ' . $arguments[0], 1);
    }

    /**
     * Important for all kinds network connection - drop connection for ensure absent leaking connections
     */
    public function __destruct()
    {
        $this->clientDb->disconnect();
    }
}