<?php
namespace Service;

/**
 * Class JsonProcService
 * Service for processing json strings
 *
 * @package Service
 */
class JsonProcService
{
    /**
     * Here we will store result array what will be computed from json string
     *
     * @var array $resultArray
     */
    public $resultArray;

    /**
     * Here we will store last error if it will be happen.
     *
     * @var string $lastError
     */
    public $lastError;

    /**
     * JsonProcService constructor.
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->resultArray = json_decode($string);

        if (!$this->resultArray) {
            $this->processError();
        }
    }

    /**
     * Processor for errors form json constructor/de-constructor
     */
    public function processError() {
        if (json_last_error()) {
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $this->lastError = 'No errors';
                    break;
                case JSON_ERROR_DEPTH:
                    $this->lastError = 'Maximum stack depth exceeded';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $this->lastError =  'Underflow or the modes mismatch';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $this->lastError = 'Unexpected control character found';
                    break;
                case JSON_ERROR_SYNTAX:
                    $this->lastError = 'Syntax error, malformed JSON';
                    break;
                case JSON_ERROR_UTF8:
                    $this->lastError = 'Malformed UTF-8 characters, possibly incorrectly encoded';
                    break;
                default:
                    $this->lastError = 'Unknown error';
                    break;
            }
        }
    }
}