<?php
namespace Processor;

use Entity\ComputedShape;
use Entity\ShapesInterface;

class ProcessorCircle extends ProcessorAbstract
{
    public function processData(ShapesInterface $shape): ComputedShape
    {
        $this->computedShape->setType($shape->getType());
        $this->computedShape->setMimeType('Content-type: image/png');

        $params = $shape->getParams();

        $im = imagecreatetruecolor($params['width'], $params['height']);
        imageantialias($im, true);

        list($r, $g, $b) = $this->colorToRGB($params['color']);
        $color = imagecolorallocate($im, $r, $g, $b);

        imagefilltoborder($im, 0, 0, $color, $color);

        list($r, $g, $b) = $this->colorToRGB($params['border']);
        $colorBorder = imagecolorallocate($im, $r, $g, $b);

        imageellipse($im, $params['coordinates']['x'], $params['coordinates']['y'], $params['width'], $params['height'], $colorBorder);

        ob_start();
        imagepng($im);
        $this->computedShape->setBinaryData(ob_get_contents());
        ob_end_clean();

        return $this->computedShape;
    }
}