<?php
namespace Processor;

use Entity\ComputedShape;
use Entity\ShapesInterface;

interface ProcessorInterface
{
    /**
     * Computing shape to binary data and fitting it to @see ComputedShape entity
     *
     * @param ShapesInterface $shape
     * @return ComputedShape
     */
    public function processData(ShapesInterface $shape):ComputedShape;
}